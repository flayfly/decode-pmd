#include <stdio.h>
#include <string.h>
#include <direct.h>
#include <malloc.h>

#define BUFFER_MAX (50*1024)

unsigned char buffer[BUFFER_MAX] = {0};
unsigned int counter = 0;

#pragma pack (1)
//坐标点
struct dot_float{
    float left;           // 左电机度数; X坐标
    float right;          // 右电机度数; Y坐标
};

//路径
struct path{
    unsigned int dot_sum;     // 像素总数
};

//数据结构
struct pmd {
    unsigned char flag;        // 格式标记
    unsigned int  path_sum;    // 路径总数
};
#pragma pack()

union char_to_float
{
    unsigned char ch[4];
    float dot;
};

unsigned char pmd_flag;
unsigned int pmd_path_sum;
unsigned int pmd_dot_sum;

struct dot_float * dot;
union char_to_float dot_left;
union char_to_float dot_right;

//工程文件的目录
////欲查找的文件，支持通配符
//const char *file_search = "C:\\Users\\fly\\Desktop\\QT\\decode-PMD\\file\\*.PMD";
////统一文件路径
//char *file_path_unity = "C:\\Users\\fly\\Desktop\\QT\\decode-PMD\\file\\";

//执行文件的目录，相对路径
//欲查找的文件，支持通配符
const char *file_search = "file\\*.PMD";
//统一文件路径
char *file_path_unity = "file\\";

//一次最多解析50个文件
char *file_name[50];
unsigned char file_num = 0;

int main()
{
    FILE *in = NULL;
    FILE *out = NULL;

    char file_in_path[100] = {0};
    char file_out_path[100] = {0};

    unsigned char value;
    unsigned char num = 0;

    //用于查找的句柄
    long handle;
    //文件信息的结构体
    struct _finddata_t fileinfo;

    printf("PMD files:\r\n");

    handle = _findfirst(file_search, &fileinfo);
    if(-1 == handle)
    {
        printf("PMD NONE!\n");
        return -1;
    }

    file_name[file_num] = (char *)malloc(sizeof(char)*10);
    if(file_name[file_num] == NULL)
    {
        printf("malloc failed!\n");
        return -1;
    }

    strcpy(file_name[file_num],fileinfo.name);
    printf("%s\n", fileinfo.name);
    file_num++;

    //循环查找其他符合的文件，直到找不到为止
    while(!_findnext(handle, &fileinfo))
    {
        file_name[file_num] = (char *)malloc(sizeof(char)*10);
        strcpy(file_name[file_num],fileinfo.name);
        printf("%s\n", fileinfo.name);
        file_num++;
        if(file_num >= 50)
        {
            printf("PMD files overflow,quit!\n");
            return -1;
        }
    }
    //关闭句柄
    _findclose(handle);

    printf("PMD files decoded:\r\n");

    for(num=0; num<file_num; num++)
    {
        strcat(file_in_path, file_path_unity);
        strcat(file_out_path, file_path_unity);

        strcat(file_in_path, file_name[num]);


        char *unity_name = ".PMD";
        unsigned char out_name_len;
        char out_name[10] = {0};
        out_name_len = strlen(file_name[num]) - strlen(strstr(file_name[num], unity_name));
        strncat(file_out_path, file_name[num], out_name_len);

        strncpy(out_name, file_name[num], out_name_len);
        printf("\n");
        printf("%s\n",out_name);

        in = fopen(file_in_path, "rb");
        if(in == NULL)
        {
            printf("open in error!\n");
            return -1;
        }

        out = fopen(file_out_path, "wt");
        if(out == NULL)
        {
            printf("open out error!\n");
            return -1;
        }

        counter = 0;
        fread(&value,1,1,in);
        while(!feof(in))
        {
            buffer[counter++] = value;
            fprintf(out, "0x%02x,",value);
            fread(&value,1,1,in);
        }

        unsigned int i=0;
        unsigned int j=0;
        unsigned int m=0;
        pmd_flag = buffer[i++];
        pmd_path_sum = buffer[i++] + (buffer[i++] << 8)+ (buffer[i++] << 16)+ (buffer[i++] << 24);

        for(j=0; j<pmd_path_sum; j++)
        {
            printf("path:%d\t", j+1);
            pmd_dot_sum = buffer[i++] + (buffer[i++] << 8)+ (buffer[i++] << 16)+ (buffer[i++] << 24);
            printf("dot_sum:%d\n", pmd_dot_sum);
            for(m=0; m<pmd_dot_sum; m++)
            {
                dot = (struct dot_float * )&buffer[i];
                i += 8;
                printf("number:%d\tleft angle:%f\tright angle:%f\n", (m+1), dot->left, dot->right);
            }
        }

        fclose(in);
        fclose(out);
        free(file_name[num]);

        memset(file_in_path, 0, sizeof(file_in_path));
        memset(file_out_path, 0, sizeof(file_out_path));
        memset(out_name, 0, sizeof(char)*10);
        memset(buffer, 0, sizeof(buffer));
    }

    unsigned char key;
    printf("Press any key to close!\n");
    getch("%c",&key);
}
